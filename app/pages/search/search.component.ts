import { Router } from "@angular/router";
import { Component, ElementRef } from "@angular/core";
import { TranslateService } from "ng2-translate";

import { Config } from "../../shared/config";

@Component({
  selector: "search-page",
  templateUrl: "pages/search/search.html",
  styleUrls: ["pages/search/search-common.css"]
})

export class SearchComponent {

    searchWord: string;

    constructor(private router: Router, private translate: TranslateService) {}

    search() {
        if(this.searchWord != undefined && this.searchWord.trim() !== "") {
            this.router.navigate(["/result", { searchWord: this.searchWord }]);
        } else {
            alert(this.translate.currentLang == Config.srCir ? Config.ENTER_A_WORD_CIR : Config.ENTER_A_WORD_LAT);
        }
    }

    switchLanguage(lang: string) {
        this.translate.use(this.translate.currentLang == Config.srCir ? Config.srLat : Config.srCir);
    }

}