
import { Router, Data, ActivatedRoute, Params } from "@angular/router";
import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { Page } from "ui/page";
import { Color } from "color";
import { View } from "ui/core/view";

import { Vokabular } from "../../shared/vokabular/vokabular";
import { VokabularService } from "../../shared/vokabular/vokabular.service";
import { TranslateService } from "ng2-translate";
import { Config } from "../../shared/config";

@Component({
  selector: "result",
  templateUrl: "pages/results/result.html",
  styleUrls: ["pages/results/result-common.css"],
  providers: [VokabularService]
})

export class ResultComponent implements OnInit {

    searchWord: string;
    vokabularList: Array<Vokabular> [];
    isLoading = false;
    lastSearchWord: string;
    langChange = false;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private vokabularService: VokabularService,
        private translate: TranslateService) {}

    ngOnInit() {
        this.searchWord = this.route.snapshot.params[Config.searchWordParam];
        this.search();
    }

    search() {
        if (this.searchWord !== undefined && this.searchWord.trim() !== "" && (this.searchWord !== this.lastSearchWord || this.langChange)) {
            this.lastSearchWord = this.searchWord;
            this.isLoading = true;
            this.vokabularService.find(this.searchWord, this.translate.currentLang).
                subscribe(data => {
                    this.vokabularList = data;
                    this.isLoading = false;
                },
                () => {
                    alert({
                      message: this.translate.currentLang == Config.srCir ? Config.ERROR_GETTING_WORD_CIR : Config.ERROR_GETTING_WORD_LAT,
                      okButtonText: "OK"
                    });
                    this.isLoading = false;
                  }
            )
        }
    }

    goToSearch() {
        this.searchWord = "";
        this.router.navigate(["/"]);
    }

    switchLanguage() {
        this.translate.use(this.translate.currentLang == Config.srCir ? Config.srLat : Config.srCir);
        this.langChange = true;
        this.search();
        this.langChange = false;
    }

}