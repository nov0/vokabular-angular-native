import { Component } from "@angular/core";

import * as Platform from "platform";
import { TranslateService } from "ng2-translate";

import { Config } from "./shared/config";

@Component({
  selector: "main",
  template: "<page-router-outlet></page-router-outlet>"
})
export class AppComponent {

  public language: string;
  
      public constructor(private translate: TranslateService) {
          this.language = Platform.device.language;
          this.translate.setDefaultLang(Config.srLat);
          this.translate.use(Config.srCir);
      }

}