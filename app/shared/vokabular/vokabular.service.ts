import { Injectable } from "@angular/core";
import { Http, Headers, Response } from "@angular/http";
import { Observable } from "rxjs/Rx";
import "rxjs/add/operator/map";

import { Config } from "../config";
import { Vokabular } from "./vokabular";

@Injectable()
export class VokabularService {

  constructor(private http: Http) {}

  find(searchWord: string, lang: string) {
    
    return this.http.get(Config.apiUrl + lang + Config.wordParam + searchWord)
    .map(res => res.json())
    .map(data => {
      let resultList = [];
      data.forEach((result) => {
        resultList.push(new Vokabular(result.word, result.origin, result.meaning));
      });
      return resultList;
    })
    .catch(this.handleErrors);
  }

  handleErrors(error: Response) {
    console.log(JSON.stringify(error.json()));
    return Observable.throw(error);
  }
}