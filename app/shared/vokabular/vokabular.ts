export class Vokabular {
    constructor(public word: string, public origin: string, public meaning: string) {}
}