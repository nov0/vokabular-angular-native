export class Config {
    static apiUrl = "http://34.208.230.122:8008/search?lang=";
    static wordParam = "&word=";
    static searchWordParam = "searchWord";
    static srCir = "sr-cir";
    static srLat = "sr-lat";
    static ERROR_GETTING_WORD_LAT = "Došlo je do greške tokom pretrage!";
    static ERROR_GETTING_WORD_CIR = "Дошло је до грешке током претраге!";
    static ENTER_A_WORD_LAT = "Molimo da unesete riječ!";
    static ENTER_A_WORD_CIR = "Молимо да унесете ријеч!";
}